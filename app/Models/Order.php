<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $dates = ['deleted_at'];
    
    protected $table = "orders";

    protected $primaryKey = "order_id";

    protected $fillable = ['order_id','order_type','shopify_order_id','email','shopify_order_number','note','token','total_price','subtotal_price','total_tax','financial_status','total_discounts','name','app_id','fulfillment_status','tags','fulfillments','is_sync','is_razorpay','created_at','updated_at','created_by','updated_by','cancelled_at','customer_name','remain_qty'];


}
