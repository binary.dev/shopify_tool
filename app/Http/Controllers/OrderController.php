<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
class OrderController extends Controller
{
    public function index(Request $request){
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1);

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (email like \"%{$search}%\""
                . " or name like \"%{$search}%\""
                . " or total_price like \"%{$search}%\""
                . " or tags like \"%{$search}%\""
                . " or fulfillment_status like \"%{$search}%\""
                . ")";
            }

            $columns = ['created_at','name','customer_name','email'];

                $order_data_list = Order::select($columns)
                                    ->whereRaw($where_str, $where_params);

                $order_list_count = count($order_data_list->get());
        

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != '' && !empty($order_data_list)){
                $order_data_list = $order_data_list->take($request->input('iDisplayLength'))
                                                    ->skip($request->input('iDisplayStart'));
            }

            if($request->input('iSortCol_0') && !empty($order_data_list)){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $order_data_list = $order_data_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
                $order_data_list = $order_data_list->get();
            }
            $response['iTotalDisplayRecords'] = $order_list_count;
            $response['iTotalRecords'] = $order_list_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $order_data_list;

            return $response;
        }
         return view('admin.order.index');
    }
}