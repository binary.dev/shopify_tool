<nav class="hk-nav hk-nav-light">
    <a href="javascript:void(0);"  class="hk-nav-close"><span class="feather-icon"><i data-feather="x"></i></span></a>
    <div class="nicescroll-bar">
        <div class="navbar-nav-wrap">
            <ul class="navbar-nav flex-column">
                <li class="nav-item ">
                        <a class="nav-link"  >
                            <i>@include('svg.order')</i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" >
                    <i>@include('svg.home')</i>
                    <span class="nav-link-text">Home</span>
                    </a>
                </li>
                <li class="nav-item ">
                        <a class="nav-link" href="javascript:void(0);" data-toggle="collapse" data-target="#order_drp">
                            <i>@include('svg.order')</i>
                            <span class="nav-link-text">Testing</span>
                        </a>
                        <ul id="order_drp" class="nav flex-column collapse collapse-level-1">
                            <li class="nav-item">
                                <ul class="nav flex-column">
                                   
                                        <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">A</span>
                                            </a>
                                        </li>
                                   
                                        <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">B</span>
                                            </a>
                                        </li>
                                    
                                        <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">C</span>
                                            </a>
                                        </li>
                                   
                                        <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">D</span>
                                            </a>
                                        </li>
                                   
                                     <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">E</span>
                                            </a>
                                        </li>
                                   
                                        <li class="nav-item ">
                                            <a class="nav-link" href="" >
                                                <i>@include('svg.order')</i>
                                                <span class="nav-link-text">F</span>
                                            </a>
                                        </li>
                                </ul>
                            </li>
                        </ul>
                    </li> 
            </ul>   
        </div>
    </div>
</nav>
<div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>