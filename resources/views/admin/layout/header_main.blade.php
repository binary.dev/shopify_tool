    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    {{ Html::style('backend/css/jquery.dataTables.min.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/responsive.dataTables.min.css',[],Config::get('constant.IS_SECURE')) }}
    
    {{ Html::style('backend/css/toggles.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/toggles-light.css',[],Config::get('constant.IS_SECURE')) }}

    <!-- Select 2 CSS -->
    {{ Html::style('backend/css/select2.min.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/multi-select.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/sweetalert.min.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/dropzone.css',[],Config::get('constant.IS_SECURE')) }}

    <!-- Daterangepicker CSS -->
    {{ Html::style('backend/css/datepicker.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/daterangepicker.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/jquery-datepicker.css',[],Config::get('constant.IS_SECURE')) }}

    <!-- File Upload CSS -->
    {{Html::style('backend/css/jquery.fileuploader.css')}}
    {{Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')}}
    {{Html::style('backend/css/bootstrap-fileupload.css')}}

    <!-- Fontawesome Icons CSS -->
    {{ Html::style('backend/css/font-awesome.min.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/all.min.css',[],Config::get('constant.IS_SECURE')) }}

    <!-- Custom CSS -->
    {{ Html::style('backend/css/toster.min.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/style.css',[],Config::get('constant.IS_SECURE')) }}
    {{ Html::style('backend/css/binary.css',[],Config::get('constant.IS_SECURE')) }}