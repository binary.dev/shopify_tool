@extends('admin.layout.layout')
@section('style')
@stop
@section('header')
<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span
        class="feather-icon"><i data-feather="menu"></i></span></a>
    <div class="container">
        <div class="hk-pg-header">
            <h4 class="hk-pg-title">Orders</h4>
        </div>
        <div class="text-align-right">
            <form id="filter" action="" method="POST">
                <a href="" class="btn btn-info btn-sm buttonOverAnimation" title="Export Listing" style="visibility: hidden;">
                    <div data-text="Export List">Export List</div>
                </a>
                {{ csrf_field()}}
                <input type="hidden" name="type_name" value="">
               
                <button class="btn btn-info btn-sm order_excel_export buttonOverAnimation" id="export_data"
                   >
                    <div data-text="">Testing</div>
                </button>
                <small class="sync_type_date"></small>
                <button class="btn btn-primary btn-sm order_sync buttonOverAnimation"
                    title="Sync" >
                    <div data-text="Sync Now">Testing</div>
                </button>
        </div>
    </div>
</nav>
@stop
@section('content')

<div class="container page_filter">
<div class="filterlabel">
<label for="Filter">Filters :</label>
</div>
<div class="filterobjects">
<div class="row">
    <div class="col-md-4">
    <input id="order_no" class="form-control input-sm number_only" type="text" placeholder="Order"
        name="order_no" value="">
    </div>
    <div class="col-md-4">
    <input id="customer_detail" class="form-control input-sm" type="text" placeholder="Customer Detail"
        name="customer_detail" value="">
    </div>
</div>
</div>
</div>
</form>
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <ul class="nav nav-tabs order-tab">
                <li class="ordertabs">
                    <a href="">
                    <span class="ordernos" id=""></span>
                    <span class="textvalue"></span>
                    </a>
                </li>
            </ul>
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <table id="master_table" class="table table-hover w-100 display">
                                <thead>
                                    <tr>
                                        <th class="tddate">ORDER DATE</th>
                                        <th class="tdorder">ORDER NO</th>
                                        <th class="tdcustomer">CUSTOMER NAME</th>
                                        <th class="tdemailid">EMAIL ID</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop
@section('script')
{{  Html::script('backend/js/jquery.form.min.js', [], Config::get('constant.IS_SECURE')) }}
{{  Html::script('backend/js/moment.min.js', [], Config::get('constant.IS_SECURE')) }}
{{  Html::script('backend/js/daterangepicker.js', [], Config::get('constant.IS_SECURE')) }}
{{  Html::script('backend/js/jquery.dataTables.min.js',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/js/dataTables.bootstrap4.min.js',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/js/dataTables.dataTables.min.js',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/js/fnstandrow.js', [], Config::get('constant.IS_SECURE')) }}
{{  Html::script('backend/js/nprogress.js')  }}
{{  Html::script('backend/js/handlebars-v4.0.12.js',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/js/select2.min.js',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/css/sweetalert.min.css',[],Config::get('constant.IS_SECURE'))  }}
{{  Html::script('backend/js/sweetalert.min.js',[],Config::get('constant.IS_SECURE'))  }}
<!-- DATATABLE CODE START -->

<script type="text/javascript">  
    var page_url = "<?= route('orders.index')?>";

    var order_table = $('#master_table').DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "autoWidth": true,
        "search": {
            "regex": false
        },
        "aaSorting": 
          [1, "desc"],
        lengthMenu: [
            [50, 100, 200, 500],
            ['50', '100', '200', '500']
        ],
        "sAjaxSource":  "<?= route('orders.index')?>",
        "aoColumns": [
            { "mData": "created_at",
                  mRender: function (v, t, o) {
                      var date = o['created_at'];
                      return moment(date).format('DD MMM, YYYY');
                   },"className": "tdemailid", bSortable: true },
            { "mData": "name", "className": "tdprice", bSortable: true },
            { "mData": 'customer_name',bSortable: false,sClass: "text-center"},
            { "mData": "email",bSortable: false,sClass: "text-center"}
                ],
        fnPreDrawCallback: function() {
            NProgress.start();
        },
        fnDrawCallback : function (oSettings) {
            NProgress.done();
        }
    });
</script>
@stop
<!-- DATATABLE CODE END -->
